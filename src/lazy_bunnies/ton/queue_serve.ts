import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type Queue from "./type/QueueServe";
import type Stable from "./type/Stable";

export const serveQueue = defineStore("serve", () => {
  const isTable = ref(0);
  let lastServe = 5;
  const test = () => {
    // ลบได้
    lastServe++;
  };
  const table = ref<Stable[]>([]);

  const tableModel = ref<Stable>({
    table: "",
  });

  const clear = () => {
    tableModel.value = {
      table: "",
    };
  };

  const queues = ref<Queue[]>([
    {
      id: 1,
      state: "กำลังรอ",
      num: 23,
      name: "ไข่ลวก",
      table: "A1",
    },
    {
      id: 2,
      state: "เสริฟแล้ว",
      num: 23,
      name: "หมาทอดกระเทียมเห็ดหอมทรงพลังตุ๋นยาจีน",
      table: "A2",
    },
    {
      id: 3,
      state: "กำลังรอ",
      num: 23,
      name: "ปลาดุกต้องสาป",
      table: "A3",
    },
    {
      id: 4,
      state: "ยกเลิก",
      num: 23,
      name: "ปลาสวรรค์พิโรธนรกแตก",
      table: "B1",
    },
  ]);
  return { queues, isTable, lastServe, table, tableModel, clear };
});
