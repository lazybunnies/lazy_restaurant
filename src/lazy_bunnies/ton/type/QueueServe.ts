export default interface QueueServe {
  id: number;
  name: string;
  table: string;
  state: string;
  num: number;
}
