import { ref } from "vue";
import { defineStore } from "pinia";
import type Table from "@/types/Table";
export const useDialogStore = defineStore("dialog", () => {
  const dialog = false;
 
  const Item = ref<Table>({
    id: -1,
    type: "",
    st: "",
  });


  return { dialog, Item };
});
