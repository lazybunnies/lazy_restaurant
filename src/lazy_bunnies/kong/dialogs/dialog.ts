import { ref } from "vue";
import { defineStore } from "pinia";
import type Table from "../../../types/Table";

export const useDialogStore = defineStore("dialog", () => {
  let dialog = false;
  const Item = ref<Table>({
    id: -1,
    type: "",
    st: "",
  });

  // const cleanTable = (table: Table) => {
  //   Item.value = { ...table };
  // };

  const callItem = (tables: Table) => {
    Item.value.id = tables.id;
    Item.value.type = tables.type;
    Item.value.st = tables.st;
    dialog = true;
  };

  return { dialog, Item, callItem };
});
