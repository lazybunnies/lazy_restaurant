import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type Menu from "./type/Menu";
import type Basket from "./type/Basket";
import type Option from "./type/Option";
import type Dialog from "./type/Dialog";
import type Order from "./type/Order";
import { useQueueStore } from "../bas/queue";

export const useMenuStore = defineStore("menu", () => {
  const isTable = ref(0);
  const table = ref("A1");
  const num = ref(1);
  const note = ref("");
  const sumList = ref(0);
  const lastMenuId = 9;
  let lastBasketId = 1;
  const BasketsCount = ref(0);
  const menuoption = ref("ทั้งหมด");

  const SumList = () => {
    for (let i = 0; i < orders.value.length; i++) {
      sumList.value += orders.value[i].price * orders.value[i].num;
    }
  };

  const confirm = () => {
    useQueueStore().addOrderToQueue();

    console.log(baskets.value.length);
    for (let i = 0; i < baskets.value.length; i++) {
      orders.value.push(baskets.value[i]);
      baskets.value.splice(i, 1);
      i--;
    }
    BasketsCount.value = baskets.value.length;
    console.log(orders);
    SumList();
  };

  const deleteItem = (id: number): void => {
    const index = baskets.value.findIndex((item) => item.id === id);
    baskets.value.splice(index, 1);
    BasketsCount.value = baskets.value.length;
  };

  const editItem = (basket: Basket) => {
    ItemDialog.value.id = basket.id;
    ItemDialog.value.name = basket.name;
    ItemDialog.value.price = basket.price;
    ItemDialog.value.img = basket.img;
    ItemDialog.value.note = basket.note;
    ItemDialog.value.num = basket.num;
    ItemDialog.value.table = basket.table;
    ItemDialog.value.type = basket.type;
    mynote.value = ItemDialog.value.note;
    dialog.value = true;
  };

  const clear = () => {
    ItemBaskets.value = {
      id: -1,
      name: "",
      price: 0,
      num: 1,
      note: "",
      table: "",
      img: "",
      type: "",
    };
    ItemDialog.value = {
      id: -1,
      name: "",
      price: 0,
      num: 1,
      note: "",
      table: "",
      img: "",
      type: "",
    };
  };

  const addNewItem = () => {
    console.log(ItemDialog.value.id);
    if (ItemDialog.value.id < 0) {
      ItemBaskets.value.name = ItemDialog.value.name;
      ItemBaskets.value.price = ItemDialog.value.price;
      ItemBaskets.value.num = ItemDialog.value.num;
      ItemBaskets.value.note = ItemDialog.value.note;
      ItemBaskets.value.table = table.value;
      ItemBaskets.value.img = ItemDialog.value.img;
      ItemBaskets.value.id = lastBasketId++;
      ItemBaskets.value.type = ItemDialog.value.type;
      baskets.value.push(ItemBaskets.value);
      BasketsCount.value = baskets.value.length;
    } else {
      const index = baskets.value.findIndex(
        (item) => item.id === ItemDialog.value.id
      );
      baskets.value[index] = ItemDialog.value;
    }
    console.log(baskets);
    clear();
  };

  const Item = ref<Menu>({
    id: -1,
    name: "",
    price: 0,
    type: "",
    img: "",
  });

  const ItemDialog = ref<Dialog>({
    id: -1,
    name: "",
    price: 0,
    num: 1,
    note: "",
    table: "",
    img: "",
    type: "",
  });

  const ItemBaskets = ref<Basket>({
    id: -1,
    name: "",
    price: 0,
    num: 0,
    note: "",
    table: "",
    img: "",
    type: "",
  });
  const mynote = ref(ItemDialog.value.note);
  const dialog = ref(false);
  const orders = ref<Order[]>([]);
  const baskets = ref<Basket[]>([]);
  const options = ref<Option[]>([
    {
      id: 1,
      Optionname: "ทั้งหมด",
    },
    {
      id: 2,
      Optionname: "ข้าว",
    },
    {
      id: 3,
      Optionname: "เมนูยำ",
    },
    {
      id: 4,
      Optionname: "ของทานเล่น",
    },
    {
      id: 5,
      Optionname: "เครื่องดื่ม",
    },
  ]);
  const menus = ref<Menu[]>([
    {
      id: 1,
      name: "ข้าวผัดปลาสลิด",
      price: 55.0,
      type: "ข้าว",
      img: "https://www.shopat24.com/blog/wp-content/webp-express/webp-images/uploads/2020/07/fried-rice-with-crispy-gourami-fish_1339-107469.jpg.webp",
    },
    {
      id: 2,
      name: "ข้าวหมูทอดกระเทียม",
      price: 55.0,
      type: "ข้าว",
      img: "https://www.taokaecafe.com/asp-bin/images/1623921030_garlic-fried-pork.jpg",
    },
    {
      id: 3,
      name: "ข้าวผัดเบคอน",
      price: 55.0,
      type: "ข้าว",
      img: "https://www.shopat24.com/blog/wp-content/webp-express/webp-images/uploads/2020/07/%E0%B8%82%E0%B9%89%E0%B8%B2%E0%B8%A7%E0%B8%9C%E0%B8%B1%E0%B8%94%E0%B9%80%E0%B8%9A%E0%B8%84%E0%B8%AD%E0%B8%99.png.webp",
    },
    {
      id: 4,
      name: "ข้าวผัดปลาแซลมอน",
      price: 60.0,
      type: "ข้าว",
      img: "https://www.shopat24.com/blog/wp-content/webp-express/webp-images/uploads/2020/07/Salmon-Fried-Rice-504x420.jpg.webp",
    },
    {
      id: 5,
      name: "ข้าวผัดต้มยำกุ้ง",
      price: 60.0,
      type: "ข้าว",
      img: "https://www.shopat24.com/blog/wp-content/webp-express/webp-images/uploads/2020/07/%E0%B8%82%E0%B9%89%E0%B8%B2%E0%B8%A7%E0%B8%9C%E0%B8%B1%E0%B8%94%E0%B8%95%E0%B9%89%E0%B8%A1%E0%B8%A2%E0%B8%B3%E0%B8%81%E0%B8%B8%E0%B9%89%E0%B8%87.jpg.webp",
    },
    {
      id: 6,
      name: "ข้าวไข่เจียว",
      price: 30.0,
      type: "ข้าว",
      img: "https://img.proxumer.com/p/1200x0/2019/01/28/zuc3nbcnqmrv.jpg",
    },
    {
      id: 7,
      name: "ข้าวผัดน้ำพริกตาแดงกุ้งสด",
      price: 55.0,
      type: "ข้าว",
      img: "https://www.shopat24.com/blog/wp-content/webp-express/webp-images/uploads/2020/07/Red-Eye-Chilli-Paste-Fried-Rice-with-Shrimp.jpg.webp",
    },
    {
      id: 8,
      name: "ข้าวหมูแดง",
      price: 50.0,
      type: "ข้าว",
      img: "https://img.wongnai.com/p/1920x0/2018/07/03/ed7ed753c9f5433fa1afa1a3bfb83796.jpg",
    },
    {
      id: 9,
      name: "ข้าวผัดกะเพรา",
      price: 50.0,
      type: "ข้าว",
      img: "https://www.taokaecafe.com/asp-bin/images/1623920395_stir-fried-basil.jpg",
    },
    {
      id: 10,
      name: "ข้าวราดคะน้าหมูกรอบ",
      price: 55.0,
      type: "ข้าว",
      img: "https://www.taokaecafe.com/asp-bin/images/1623920597_crispy-pork-kale.jpg",
    },
    {
      id: 11,
      name: "ยำคะน้ากรอบกุ้งสด",
      price: 55.0,
      type: "เมนูยำ",
      img: "https://img.thaibuffer.com/u/2017/surauch/cooking/f2_11.jpg",
    },
    {
      id: 12,
      name: "ยำหัวปลีกุ้งสด",
      price: 55.0,
      type: "เมนูยำ",
      img: "https://img.thaibuffer.com/u/2017/surauch/cooking/f3_10.jpg",
    },
    {
      id: 13,
      name: "ยำแหนมข้าวทอด",
      price: 50.0,
      type: "เมนูยำ",
      img: "https://img.thaibuffer.com/u/2017/surauch/cooking/f4_9.jpg",
    },
    {
      id: 14,
      name: "ยำขนมจีนรวมมิตร",
      price: 50.0,
      type: "เมนูยำ",
      img: "https://img.thaibuffer.com/u/2017/surauch/cooking/f5_9.jpg",
    },
    {
      id: 15,
      name: "ยำมะม่วงปูม้า",
      price: 55.0,
      type: "เมนูยำ",
      img: "https://img.thaibuffer.com/u/2017/surauch/cooking/f7_6.jpg",
    },
    {
      id: 16,
      name: "ยำมะม่วงปลากรอบ",
      price: 55.0,
      type: "ข้าว",
      img: "https://img.thaibuffer.com/u/2017/surauch/cooking/f8_6.jpg",
    },
    {
      id: 17,
      name: "ยำถั่วพูกุ้งสด",
      price: 55.0,
      type: "ข้าว",
      img: "https://img.thaibuffer.com/u/2017/surauch/cooking/f9_5.jpg",
    },
    {
      id: 18,
      name: "เกี๊ยวทอดไส้ผักโขมแฮมชีส",
      price: 40.0,
      type: "ของทานเล่น",
      img: "https://www.hongthongrice.com/v2/wp-content/uploads/2018/12/HongthongRice-Blog-10Menu-1.jpg",
    },
    {
      id: 19,
      name: "โคโรเกะทูน่าสอดไส้ชีส",
      price: 45.0,
      type: "ของทานเล่น",
      img: "https://www.hongthongrice.com/v2/wp-content/uploads/2018/12/HongthongRice-Blog-10Menu-2.jpg",
    },
    {
      id: 20,
      name: "ไก่เขย่า",
      price: 50.0,
      type: "ของทานเล่น",
      img: "https://www.hongthongrice.com/v2/wp-content/uploads/2018/12/HongthongRice-Blog-10Menu-3.jpg",
    },
    {
      id: 21,
      name: "เบคอนม้วนไส้กรอกปิ้งเครื่องลาบ",
      price: 40.0,
      type: "ของทานเล่น",
      img: "https://www.hongthongrice.com/v2/wp-content/uploads/2018/12/HongthongRice-Blog-10Menu-4-.jpg",
    },
    {
      id: 22,
      name: "หมูกระเบื้อง",
      price: 45.0,
      type: "ของทานเล่น",
      img: "https://www.hongthongrice.com/v2/wp-content/uploads/2018/12/HongthongRice-Blog-10Menu-6.jpg",
    },
    {
      id: 23,
      name: "น้ำแข็งถัง",
      price: 10.0,
      type: "เครื่องดื่ม",
      img: "https://f.ptcdn.info/301/075/000/r1rxry2utuUA6UBR1JR-o.jpg",
    },
    {
      id: 24,
      name: "โค้ก ออริจินัล 2ลิตร",
      price: 45.0,
      type: "เครื่องดื่ม",
      img: "https://sentosakhonkaen.com/wp-content/uploads/2021/07/S_0032_8851959142011.jpg",
    },
    {
      id: 25,
      name: "น้ำดื่มสิงห์ 1.5ลิตร",
      price: 20.0,
      type: "เครื่องดื่ม",
      img: "https://ustatic.priceza.com/img/productgroup/83661-1-l.jpg",
    },
  ]);

  const Additem = (menus: Menu) => {
    // Item.value = { ...menus };
    ItemDialog.value.img = menus.img;
    ItemDialog.value.name = menus.name;
    ItemDialog.value.price = menus.price;
    ItemDialog.value.type = menus.type;
    dialog.value = true;
  };

  return {
    menus,
    isTable,
    dialog,
    Additem,
    Item,
    num,
    addNewItem,
    note,
    lastBasketId,
    baskets,
    BasketsCount,
    deleteItem,
    editItem,
    ItemDialog,
    mynote,
    clear,
    orders,
    confirm,
    options,
    menuoption,
    table,
    sumList,
    SumList,
  };
});
