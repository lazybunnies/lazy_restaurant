export default interface Basket {
  id: number;
  name: string;
  price: number;
  num: number;
  note: string;
  table: string;
  img: string;
  type: string;
}
