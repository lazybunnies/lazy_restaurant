export default interface Menu {
  id: number;
  name: string;
  price: number;
  type: string;
  img: string;
}
