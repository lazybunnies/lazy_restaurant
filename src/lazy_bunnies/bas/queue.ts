import { ref } from "vue";
import { defineStore } from "pinia";
import type Queue from "./type/Queue";
import type Cook from "./type/Cook";
import { useMenuStore } from "../game/menu";
import { serveQueue } from "../ton/queue_serve";
import type QueueServe from "../ton/type/QueueServe";

export const useQueueStore = defineStore("queue", () => {
  const menuStore = useMenuStore();
  const serveStore = serveQueue();
  const isTable = ref(0);
  const lastQueueId = 4;
  // let lastCookId = 1;

  const addItemCook = (cook: Cook, id: number): void => {
    const index = queues.value.findIndex((item) => item.id === id);
    cooks.value.push(cook);
    queues.value.splice(index, 1);
  };

  const cancelOrder = (id: number): void => {
    const index = queues.value.findIndex((item) => item.id === id);
    queues.value.splice(index, 1);
  };

  const competeCook = (id: number): void => {
    const index = cooks.value.findIndex((item) => item.id === id);
    ServeItem.value.id = serveStore.lastServe++;
    ServeItem.value.name = cooks.value[index].name;
    ServeItem.value.table = cooks.value[index].table;
    ServeItem.value.num = cooks.value[index].num;
    serveStore.queues.push(ServeItem.value);
    clear();
    cooks.value.splice(index, 1);
  };

  const returnItemQueue = (queue: Queue, id: number): void => {
    const index = cooks.value.findIndex((item) => item.id === id);
    queues.value.push(queue);
    cooks.value.splice(index, 1);
  };

  const cooks = ref<Cook[]>([]);
  const queues = ref<Queue[]>([]);

  const ServeItem = ref<QueueServe>({
    id: -1,
    name: "",
    table: "",
    state: "กำลังรอ",
    num: 0,
  });

  const addOrderToQueue = () => {
    for (let i = 0; i < menuStore.baskets.length; i++) {
      if (menuStore.baskets[i].type == "เครื่องดื่ม") {
        ServeItem.value.id = serveStore.lastServe++;
        ServeItem.value.name = menuStore.baskets[i].name;
        ServeItem.value.table = menuStore.baskets[i].table;
        ServeItem.value.num = menuStore.baskets[i].num;
        serveStore.queues.push(ServeItem.value);
        clear();
      } else {
        queues.value.push(menuStore.baskets[i]);
      }
    }
  };

  const clear = () => {
    ServeItem.value = {
      id: -1,
      name: "",
      table: "",
      state: "กำลังรอ",
      num: 0,
    };
  };

  return {
    queues,
    isTable,
    cancelOrder,
    addItemCook,
    cooks,
    competeCook,
    returnItemQueue,
    addOrderToQueue,
  };
});
