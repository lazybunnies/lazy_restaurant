export default interface Queue {
  // id: number;
  // food: string;
  // option: string;
  // detail: string;
  // table: string;
  id: number;
  name: string;
  price: number;
  num: number;
  note: string;
  table: string;
  img: string;
  type: string;
}
