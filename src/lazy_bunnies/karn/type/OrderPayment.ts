export default interface OrderPayment {
  id: number;
  name: string;
  price: number;
  num: number;
  note: string;
  table: string;
  img: string;
  type: string;
  discount: number;
}
