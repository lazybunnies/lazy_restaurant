import { ref } from "vue";
import { defineStore } from "pinia";
import type Table from "@/types/Table";
import { useMenuStore } from "../game/menu";
import type Queue from "../bas/type/Queue";
import type OrderPayment from "./type/OrderPayment";

export const usePaymentStore = defineStore("payment", () => {
  const menuStore = useMenuStore();
  const queues = ref<Queue[]>([]);
  let dialog = false;
  const Item = ref<Table>({
    id: -1,
    type: "",
    st: "",
  });
  const OrderPay = ref<OrderPayment[]>([
    {
      id: 1,
      name: "ข้าวผัดปลาสลิด",
      price: 55.0,
      num: 1,
      note: "",
      table: "",
      img: "https://www.shopat24.com/blog/wp-content/webp-express/webp-images/uploads/2020/07/fried-rice-with-crispy-gourami-fish_1339-107469.jpg.webp",
      type: "ข้าว",
      discount: 5.0,
    },
  ]);
  const callItem = (tables: Table) => {
    Item.value.id = tables.id;
    Item.value.type = tables.type;
    Item.value.st = tables.st;
    dialog = true;
  };
  const addOrderToQueue = () => {
    for (let i = 0; i < menuStore.baskets.length; i++) {
      queues.value.push(menuStore.baskets[i]);
    }
  };
  return { dialog, Item, callItem, addOrderToQueue, OrderPay };
});
