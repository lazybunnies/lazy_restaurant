import { ref } from "vue";
import { defineStore } from "pinia";
import type Table from "../types/Table";

export const useTableStore = defineStore("table", () => {
  // const lastId = 5;
  const ItemDialog = ref<Table>({
    id: -1,
    type: "",
    st: "",
  });

  const ItemChangeTable = ref<Table>({
    id: 0,
    type: "",
    st: "",
  });

  const dialog = ref(false);
  const tables = ref<Table[]>([
    //A
    { id: 1, type: "A", st: "red" },
    { id: 2, type: "A", st: "red" },
    { id: 3, type: "A", st: "green" },
    { id: 4, type: "A", st: "green" },
    { id: 5, type: "A", st: "yellow" },
    //B
    { id: 1, type: "B", st: "green" },
    { id: 2, type: "B", st: "green" },
    { id: 3, type: "B", st: "red" },
    { id: 4, type: "B", st: "red" },
    { id: 5, type: "B", st: "yellow" },
    { id: 6, type: "B", st: "red" },
    { id: 7, type: "B", st: "red" },
    { id: 8, type: "B", st: "green" },
    { id: 9, type: "B", st: "green" },
    { id: 10, type: "B", st: "yellow" },
    //C
    { id: 1, type: "C", st: "green" },
    { id: 2, type: "C", st: "red" },
    { id: 3, type: "C", st: "yellow" },
    { id: 4, type: "C", st: "red" },
    { id: 5, type: "C", st: "green" },
    //D
    { id: 1, type: "D", st: "green" },
    { id: 2, type: "D", st: "red" },
  ]);

  const Additem = (tables: Table) => {
    ItemDialog.value.id = tables.id;
    ItemDialog.value.type = tables.type;
    ItemDialog.value.st = tables.st;
    dialog.value = true;
  };

  const redTableStatus = () => {
    console.log("พาย");
    if (ItemDialog.value.st === "green") {
      const index = tables.value.findIndex(
        (item) =>
          item.id === ItemDialog.value.id && item.type === ItemDialog.value.type
      );
      tables.value[index].st = "red";
    }

    dialog.value = false;
  };

  // const AredItems = tables.value.filter(item => item.st === 'red' && item.type === 'A' );
  // const AredItemsCount = AredItems.length;
  // const AyellowItems = tables.value.filter(item => item.st === 'yellow' && item.type === 'A' );
  // const AyellowItemsCount = AyellowItems.length;
  // const AgreenItems = tables.value.filter(item => item.st === 'green' && item.type === 'A' );
  // const AgreenItemsCount = AgreenItems.length;
  // const BredItems = tables.value.filter(item => item.st === 'red' && item.type === 'B' );
  // const BredItemsCount = BredItems.length;
  // const ByellowItems = tables.value.filter(item => item.st === 'yellow' && item.type === 'B' );
  // const ByellowItemsCount = ByellowItems.length;
  // const BgreenItems = tables.value.filter(item => item.st === 'green' && item.type === 'B' );
  // const BgreenItemsCount = BgreenItems.length;
  // const CredItems = tables.value.filter(item => item.st === 'red' && item.type === 'C' );
  // const CredItemsCount = CredItems.length;
  // const CyellowItems = tables.value.filter(item => item.st === 'yellow' && item.type === 'C' );
  // const CyellowItemsCount = CyellowItems.length;
  // const CgreenItems = tables.value.filter(item => item.st === 'green' && item.type === 'C' );
  // const CgreenItemsCount = CgreenItems.length;
  // const DredItems = tables.value.filter(item => item.st === 'red' && item.type === 'D' );
  // const DredItemsCount = DredItems.length;
  // const DyellowItems = tables.value.filter(item => item.st === 'yellow' && item.type === 'D' );
  // const DyellowItemsCount = DyellowItems.length;
  // const DgreenItems = tables.value.filter(item => item.st === 'green' && item.type === 'D' );
  // const DgreenItemsCount = DgreenItems.length;

  const changeTableStatus = () => {
    if (ItemDialog.value.st === "yellow") {
      const index = tables.value.findIndex(
        (item) =>
          item.id === ItemDialog.value.id && item.type === ItemDialog.value.type
      );
      tables.value[index].st = "green";
    } else if (ItemDialog.value.st === "red") {
      const index = tables.value.findIndex(
        (item) =>
          item.id === ItemDialog.value.id && item.type === ItemDialog.value.type
      );
      tables.value[index].st = "yellow";
    } else if (ItemDialog.value.st === "green") {
      const index = tables.value.findIndex(
        (item) =>
          item.id === ItemDialog.value.id && item.type === ItemDialog.value.type
      );
      tables.value[index].st = "red";
    }

    dialog.value = false;
  };

  const showTableName = (tables: Table) => {
    ItemDialog.value.id = tables.id;
    ItemDialog.value.type = tables.type;
    ItemDialog.value.st = tables.st;
  };

  const changeTable = (tables: Table) => {
    ItemChangeTable.value.id = tables.id;
    ItemChangeTable.value.type = tables.type;
    ItemChangeTable.value.st = tables.st;
  };

  const confirmChange = () => {
    if (ItemDialog.value.st === "red") {
      const index = tables.value.findIndex(
        (item) =>
          item.id === ItemDialog.value.id && item.type === ItemDialog.value.type
      );
      tables.value[index].st = "yellow";
    }

    if (ItemChangeTable.value.st === "green") {
      const index = tables.value.findIndex(
        (item) =>
          item.id === ItemChangeTable.value.id &&
          item.type === ItemChangeTable.value.type
      );
      tables.value[index].st = "red";
    }
  };

  return {
    tables,
    Additem,
    dialog,
    ItemDialog,
    changeTableStatus,
    redTableStatus,
    changeTable,
    showTableName,
    ItemChangeTable,
    confirmChange,
  };
});
